import './App.css';
import Menu from "./Components/Menu/Menu";
import {Route, Switch} from "react-router-dom";
import Home from "./Container/Home/Home";
import Add from "./Container/Add/Add";
import About from "./Container/About/About";
import Contacts from "./Container/Contacts/Contacts";
import Post from "./Container/Post/Post";
import EditPage from "./Components/EditPage/EditPage";

function App() {
    return (
        <div className="container">
            <div className="header">
                <div className="myBlog"><p>My blog</p></div>
                <div className="menu"><Menu/></div>
            </div>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/Add" component={Add}/>
                <Route path="/About" component={About}/>
                <Route path="/Contacts" component={Contacts}/>
                <Route path="/post/:id" exact component={Post}/>
                <Route path="/post/:id/edit" component={EditPage}/>
            </Switch>
        </div>
    );
}

export default App;
