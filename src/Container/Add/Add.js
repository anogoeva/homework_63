import React, {useState} from 'react';
import './Add.css';
import axios from "axios";
import dayjs from "dayjs";

const Add = ({history}) => {
    const [postBody, setPostBody] = useState({
        date: '',
        message: '',
        title: ''
    });
    const onInputTextareaChange = e => {
        const {name, value} = e.target;
        setPostBody(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const addPost = () => {
        const fetchData = async () => {
            axios({
                method: 'post',
                url: 'https://homework-63-d4785-default-rtdb.firebaseio.com/posts.json',
                headers: {},
                data: {
                    title: postBody.title,
                    message: postBody.message,
                    date: dayjs().format('DD-MM-YYYY HH:mm:ss')
                }
            })
                .then(r => history.replace('/'));
        };
        fetchData().catch(e => console.error(e));
    };

    return (
        <div className='AddPost'>
            <h4>Add new post</h4>
            <form>
                <div className="AddInput">
                    <p><label htmlFor="Title" className="AddLabel">Title</label></p>
                    <input
                        className="form"
                        name="title"
                        type="text"
                        value={postBody.title}
                        onChange={onInputTextareaChange}
                    />
                </div>
                <div>
                    <p><label htmlFor="Description" className="AddLabel">Description</label></p>
                    <textarea
                        name="message"
                        value={postBody.message}
                        onChange={onInputTextareaChange} cols="55" rows="5"
                        className="form"/></div>
                <button onClick={addPost} className="AddButton" type="button">Save</button>
            </form>
        </div>
    );
};

export default Add;