import React from 'react';
import './About.css'

const About = () => {
    return (
        <div className="blockAbout">
            <div className="blockAboutMission">
                <div className="blockAboutInner">
                    <h5>our mission</h5>
                    <h2>Give people the power to build community and bring the world closer together</h2>
                </div>
            </div>
            <div className="blockAboutPrinciples">
                <div className="ourPrinciples">
                    <h3>Our principles</h3>
                    <h5>Our principles are what we stand for. They are beliefs we hold deeply and make tradeoffs to
                        pursue.</h5>
                    <div className="boxes">
                        <div className="box">
                            <h5>Give People a Voice</h5>
                            <p>People deserve to be heard and to have a voice — even when that means defending the right
                                of people we disagree with.</p>
                        </div>
                        <div className="box">
                            <h5>Serve Everyone</h5>
                            <p>We work to make technology accessible to everyone, and our business model is ads so our
                                services can be free.</p>
                        </div>
                        <div className="box">
                            <h5>Build Connection and Community</h5>
                            <p>Our services help people connect, and when they’re at their best, they bring people
                                closer together.</p>
                        </div>
                        <div className="box">
                            <h5>Keep People Safe and Protect Privacy</h5>
                            <p>We have a responsibility to promote the best of what people can do together by keeping
                                people safe and preventing harm.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div className="OurTeam">Our team</div>
            <div className="team-grid">
                <div className="team_item active">
                    <div className="image_1">
                    </div>
                    <h3>Alex</h3><p>CEO</p>
                </div>
                <div className="team_item active">
                    <div className="image_2">
                    </div>
                    <h3>Olga</h3><p>Com Director</p>
                </div>
            </div>
        </div>
    );
};

export default About;