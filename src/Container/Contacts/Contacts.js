import React from 'react';
import './Contacts.css'

const Contacts = () => {
    return (
        <div className="blockContact">
            <h3>Контактная информация</h3>
            <div className="boxesContact">
                <div className="bbox bbox-1">
                    <img
                        src="https://cdn3.iconfinder.com/data/icons/ecommerce-24-monocolor-filled/24/Ecommerce_Glyph_Contact_Us_Location_Address-512.png"
                        alt="adress" width="50" height="50"/>
                    <h5>Адрес</h5>
                    <p>Кыргызстан г.Бишкек
                        <br/>
                        Ул.Маяковского 205</p>
                </div>
                <div className="bbox bbox-2">
                    <img
                        src="https://media.istockphoto.com/vectors/black-phone-icon-on-white-background-vector-illustration-vector-id1130588880?k=6&m=1130588880&s=612x612&w=0&h=05JlFemtKaRv0jFmOgH52SV9wGMTSekPAyPRyi_aST8="
                        alt="phone" width="50" height="50"/>
                    <h5>Телефоны</h5>
                    <p>+996 778 95 59 59
                        <br/>
                        +996 776 87 87 95</p>
                </div>
                <div className="bbox bbox-3">
                    <img
                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS10sXUPcODWCwePZ-wEWZx1BoczFgid58tug&usqp=CAU"
                        alt="e-mail" width="50" height="50"/>
                    <h5>E-mail</h5>
                    <p>block@gmail.com</p>
                </div>
                <div className="bbox bbox-4">
                    <img
                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTvgYayU2dvy9p2qUh5kxAt0dqDDZFeMKK3IQ&usqp=CAU"
                        alt="schedule" width="50" height="50"/>
                    <h5>График</h5>
                    <p>Пн-Пт: с 09.00 - 20.00 <br/>
                        Сб: с 10.00 - 15.00
                    </p>
                </div>
            </div>
            <div className="toContact">Свяжитесь с нами!</div>
        </div>
    );
};

export default Contacts;