import React, {useEffect, useState} from 'react';
import axios from "axios";
import {NavLink, useHistory} from "react-router-dom";
import './Post.css'

const Post = ({match, history}) => {
    const histories = useHistory();
    const [post, setPost] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://homework-63-d4785-default-rtdb.firebaseio.com/posts/' + match.params.id + '.json');
            setPost(response.data);
        };
        fetchData().catch(console.error);
    }, [match.params.id])

    const goHome = () => {
        axios.delete('https://homework-63-d4785-default-rtdb.firebaseio.com/posts/' + match.params.id + '.json').then(r => history.replace('/'));
    };

    return post && (
        <div className='post'>
            <p className="postData">Created on: {post.date}</p>
            <h4>Title: {post.title}</h4>
            <p>{post.message}</p>
            <div className="postInfo">
                <NavLink className="EditButton" to={histories.location.pathname + '/edit'}>
                    Edit
                </NavLink>
                <button type="button" onClick={goHome} className="EditButton">Delete</button>
            </div>
        </div>
    );
};

export default Post;