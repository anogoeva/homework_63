import React, {useEffect, useState} from 'react';
import './Home.css';
import axios from "axios";
import {NavLink} from "react-router-dom";

const Home = () => {
    const [posts, setPosts] = useState({});
    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://homework-63-d4785-default-rtdb.firebaseio.com/posts.json');
            setPosts(response.data);
        };

        fetchData().catch(console.error);
    }, [])

    const postList = Object.entries(posts).map(([key, value]) => {
        return (
            <div key={key} className="post">
                <div className="postDate">Created on: {value.date}</div>
                <div className="postMessage">Title: {value.title}</div>
                <NavLink className="postButton" to={'/post/' + key}>
                    Read More
                </NavLink>
            </div>
        );
    })

    return (
        <article className="article">
            {postList}
        </article>
    );
};

export default Home;