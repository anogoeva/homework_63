import React from 'react';
import {NavLink} from "react-router-dom";
import './Menu.css'

const Menu = () => {
    return (
        <div>
            <ul>
                <li>
                    <NavLink
                        to='/'>Home</NavLink>
                </li>
                <li>
                    <NavLink
                        to='/Add'>Add</NavLink>
                </li>
                <li>
                    <NavLink
                        to='/About'>About</NavLink>
                </li>
                <li>
                    <NavLink
                        to='Contacts'>Contacts</NavLink>
                </li>
            </ul>
        </div>
    );
};

export default Menu;