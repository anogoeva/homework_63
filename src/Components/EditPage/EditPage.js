import React, {useEffect, useState} from 'react';
import './EditPage.css'
import axios from "axios";
import dayjs from "dayjs";

const EditPage = ({match, history}) => {

    const [post, setPost] = useState({title: '', message: ''});

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('https://homework-63-d4785-default-rtdb.firebaseio.com/posts/' + match.params.id + '.json');
            setPost(response.data);
        };
        fetchData().catch(console.error);
    }, [match.params.id]);

    const onInputTextareaChange = e => {
        const {name, value} = e.target;
        setPost(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const editPost = () => {
        const fetchData = async () => {
            axios({
                method: 'put',
                url: 'https://homework-63-d4785-default-rtdb.firebaseio.com/posts/' + match.params.id + '.json',
                headers: {},
                data: {
                    title: post.title,
                    message: post.message,
                    date: dayjs().format('DD-MM-YYYY HH:mm:ss')
                }
            })
                .then(r => history.replace('/'));
        };
        fetchData().catch(e => console.error(e));
    };

    return (
        <div className='EditPost'>
            <h4>Edit post</h4>
            <form>
                <div className="EditInput">
                    <p><label htmlFor="Title" className="AddLabel">Title</label></p>
                    <input
                        className="form"
                        name="title"
                        type="text"
                        value={post.title}
                        onChange={onInputTextareaChange}
                    />
                </div>
                <div>
                    <p><label htmlFor="Description" className="AddLabel">Description</label></p>
                    <textarea
                        name="message"
                        value={post.message}
                        onChange={onInputTextareaChange} cols="55" rows="5"
                        className="form"/></div>
                <button onClick={editPost} className="EditButton" type="button">Save</button>
            </form>
        </div>
    );
};

export default EditPage;